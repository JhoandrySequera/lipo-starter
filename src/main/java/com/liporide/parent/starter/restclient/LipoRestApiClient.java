package com.liporide.parent.starter.restclient;


import com.liporide.parent.starter.util.BasicConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

@Component
@ComponentScan
public class LipoRestApiClient {

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    public RestOperations getRestTemplate() {
        return restTemplateBuilder.build();
    }

    public HttpEntity getEntityWithHeaders(Object bodyObject, String headerApp){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set(BasicConfiguration.HEADER_AUTH.getValue(), BasicConfiguration.HEADER_AUTH.getValue());
        headers.set(BasicConfiguration.HEADER_APP.getValue(), BasicConfiguration.HEADER_INTERNAL_API.getValue().concat(headerApp));
        return new HttpEntity<>(bodyObject, headers);
    }

    public HttpEntity getEntityWithHeaders(String headerApp){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set(BasicConfiguration.HEADER_AUTH.getValue(), BasicConfiguration.HEADER_AUTH.getValue());
        headers.set(BasicConfiguration.HEADER_APP.getValue(), BasicConfiguration.HEADER_INTERNAL_API.getValue().concat(headerApp));
        return new HttpEntity<>(headers);
    }

    public HttpEntity getEntityWithHeaders(String headerAuthorization, String headerApp){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set(BasicConfiguration.HEADER_AUTH.getValue(), headerAuthorization);
        headers.set(BasicConfiguration.HEADER_APP.getValue(), headerApp);
        return new HttpEntity<>(headers);
    }

    public HttpEntity getEntityWithHeaders(Object bodyObject, String headerAuthorization, String headerApp){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set(BasicConfiguration.HEADER_AUTH.getValue(), headerAuthorization);
        headers.set(BasicConfiguration.HEADER_APP.getValue(), headerApp);
        return new HttpEntity<>(bodyObject, headers);
    }
}
