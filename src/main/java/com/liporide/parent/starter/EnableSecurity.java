package com.liporide.parent.starter;

import com.liporide.parent.starter.filter.WebSecurityConfiguration;
import com.liporide.parent.starter.filter.firebase.FirebaseConnection;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ComponentScan
@Import({WebSecurityConfiguration.class, FirebaseConnection.class})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableSecurity {

	String[] ignoring() default {};

	String[] corsOrigins() default {};

	String[] apps() default {"driver", "passenger"};
}