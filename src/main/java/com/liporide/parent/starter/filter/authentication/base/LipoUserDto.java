package com.liporide.parent.starter.filter.authentication.base;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
public class LipoUserDto implements Serializable {

    private static final long serialVersionUID = -6378067854349103643L;
    private String uid;
    private String subject;
    private String email;
    private String phone;
    private String project; // temporal solution: claims still not set by mobile team.
    private Collection<? extends GrantedAuthority> authorities;
}
