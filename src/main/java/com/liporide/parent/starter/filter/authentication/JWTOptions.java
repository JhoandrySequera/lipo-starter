package com.liporide.parent.starter.filter.authentication;


import com.google.firebase.auth.FirebaseAuth;
import com.liporide.parent.starter.filter.firebase.AuthorizerConfiguration;
import com.liporide.parent.starter.filter.authentication.restClient.BackOfficeRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
@ComponentScan
public class JWTOptions {

    private static final Logger LOGGER = LoggerFactory.getLogger(JWTOptions.class);

    @Autowired
    @Qualifier("driverFirebaseAuth")
    private FirebaseAuth driverFirebaseAuth;

    @Autowired
    @Qualifier("passengerFirebaseAuth")
    private FirebaseAuth passengerFirebaseAuth;

    @Autowired
    private BackOfficeRestClient backOfficeAuthorizer;

    @Autowired
    private FirebaseAuthorizer firebaseAuthorizer;

    public Optional<Map<String, Object>> validate(String app, String token) {
        if(app.equals(AuthorizerConfiguration.BACKOFFICE.applicationName())) {
            return backOfficeAuthorizer.isValid(token);
        } else if(app.equals(AuthorizerConfiguration.DRIVER.applicationName())) {
            return firebaseAuthorizer.validateToken(driverFirebaseAuth, token);
        } else if(app.equals(AuthorizerConfiguration.PASSENGER.applicationName())) {
            return firebaseAuthorizer.validateToken(passengerFirebaseAuth, token);
        }

       throw new RuntimeException(String.format("Can't select app to validate token with this name %s", app));
    }
}
