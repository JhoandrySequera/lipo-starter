package com.liporide.parent.starter.filter.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.liporide.parent.starter.EnableSecurity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static com.liporide.parent.starter.filter.firebase.AuthorizerConfiguration.DRIVER;
import static com.liporide.parent.starter.filter.firebase.AuthorizerConfiguration.PASSENGER;

@Configuration
@ComponentScan
public class FirebaseConnection implements ImportAware {

    private FirebaseApp driverFirebaseApp;
    private FirebaseApp passengerFirebaseApp;

    @Value("${spring.profiles.active}")
    private String profile;

    private String[] apps = {};

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        Map<String, Object> annotationAttributes = importMetadata.getAnnotationAttributes(EnableSecurity.class.getName(), false);
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(annotationAttributes);
        apps = attributes.getStringArray("apps");
    }

    @PostConstruct
    public void firebaseInit() throws IOException {
        for(String app : apps) {
            if(DRIVER.applicationName().equals(app)) {
                driverFirebaseApp = loadConfiguration(DRIVER);
            } else if(PASSENGER.applicationName().equals(app)) {
                passengerFirebaseApp = loadConfiguration(PASSENGER);
            }
        }
    }

    @Bean
    public FirebaseAuth driverFirebaseAuth() {
        return FirebaseAuth.getInstance(driverFirebaseApp);
    }

    @Bean
    public FirebaseAuth passengerFirebaseAuth() {
        return FirebaseAuth.getInstance(passengerFirebaseApp);
    }

    @Bean
    public FirebaseMessaging firebaseDriverMessaging() { return FirebaseMessaging.getInstance(driverFirebaseApp); }

    @Bean
    public FirebaseMessaging firebasePassengerMessaging() { return FirebaseMessaging.getInstance(passengerFirebaseApp); }

    private FirebaseApp loadConfiguration(AuthorizerConfiguration authorizer) throws IOException {
        InputStream inputStream = FirebaseConnection.class.getClassLoader().getResourceAsStream(authorizer.credentialPath(profile));
        FirebaseOptions firebaseOptions = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials.fromStream(inputStream))
                .setDatabaseUrl(authorizer.dataBaseUrl(profile)).build();
        return FirebaseApp.initializeApp(firebaseOptions, authorizer.applicationName());
    }
}
