package com.liporide.parent.starter.filter;

import com.liporide.parent.starter.EnableSecurity;
import com.liporide.parent.starter.filter.configuration.JwtAuthenticationEntryPoint;
import com.liporide.parent.starter.filter.configuration.WebSecurityFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.Assert;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Map;

@Configuration
@ComponentScan
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter implements ImportAware {

	@Autowired
	private JwtAuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	private WebSecurityFilter webSecurityFilter;

	private String[] ignoring = {};
	private String[] corsOrigins = {};

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
				.csrf().disable()
				.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		if(isIgnoringMatchingNotEmpty()) {
			httpSecurity.authorizeRequests()
					.antMatchers(ignoring).permitAll()
					.anyRequest().authenticated();
		} else {
			httpSecurity.authorizeRequests().anyRequest().authenticated();
		}

		httpSecurity
				.addFilterBefore(webSecurityFilter, UsernamePasswordAuthenticationFilter.class);

		httpSecurity
				.headers()
				.frameOptions().sameOrigin()
				.cacheControl();
	}

	@Override
	public void configure (WebSecurity web) throws Exception {
		web.ignoring()
			.antMatchers(
				HttpMethod.GET,
				"/",
				"/*.html",
				"/favicon.ico",
				"/**/*.html",
				"/**/*.css",
				"/**/*.js",
				"/swagger-resources/**",
				"/swagger-ui.html",
				"/v2/api-docs",
				"/webjars/**");
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

		for (String path : corsOrigins) {
			CorsConfiguration corsConfiguration =  new CorsConfiguration()
					.applyPermitDefaultValues();
			corsConfiguration.addAllowedHeader("Access-Control-Allow-Origin");
			source.registerCorsConfiguration(path,corsConfiguration);
		}

		return source;
	}

	@Override
	public void setImportMetadata(AnnotationMetadata importMetadata) {
		Map<String, Object> annotationAttributes = importMetadata.getAnnotationAttributes(EnableSecurity.class.getName(), false);
		AnnotationAttributes attributes = AnnotationAttributes.fromMap(annotationAttributes);
		ignoring = attributes.getStringArray("ignoring");
		corsOrigins = attributes.getStringArray("corsOrigins");
		corsConfigurationSource();
	}

	private boolean isIgnoringMatchingNotEmpty() {
		return Arrays.stream(ignoring).count() > 0;
	}
}
