package com.liporide.parent.starter.filter.firebase;

import com.liporide.parent.starter.util.BasicConfiguration;

public enum AuthorizerConfiguration {

    DRIVER {
        @Override
        public String applicationName()  { return "driver"; }

        @Override
        public String dataBaseUrl(String profile) {
            if(profile.startsWith(BasicConfiguration.LOCAL_PROFILE.getValue()))
                return "https://cabbieone-debug.firebaseio.com";
            else if(profile.startsWith(BasicConfiguration.DEVELOP_PROFILE.getValue()))
                return "https://cabbieone-debug.firebaseio.com";
            else if(profile.startsWith(BasicConfiguration.STAGE_PROFILE.getValue()))
                return "https://lipo-stage-driver.firebaseio.com";
            else if(profile.startsWith(BasicConfiguration.PRODUCTION_PROFILE.getValue()))
                return "https://lipo-driver-production.firebaseio.com";
            else
                throw new NullPointerException("Cant get environment credentials");

        }

        @Override
        public String credentialPath(String profile) {
            String prefix = profile.startsWith(BasicConfiguration.LOCAL_PROFILE.getValue())?
                    BasicConfiguration.DEVELOP_PROFILE.getValue() : profile;
            return "firebase/driver-"+prefix+".json";
        }
    },
    PASSENGER {
        @Override
        public String applicationName() {
            return "passenger";
        }

        @Override
        public String dataBaseUrl(String profile) {
            if(profile.startsWith(BasicConfiguration.LOCAL_PROFILE.getValue()))
                return "https://cabbie-passenger-debug.firebaseio.com";
            else if(profile.startsWith(BasicConfiguration.DEVELOP_PROFILE.getValue()))
                return "https://cabbie-passenger-debug.firebaseio.com";
            else if(profile.startsWith(BasicConfiguration.STAGE_PROFILE.getValue()))
                return "https://lipo-passenger-stage.firebaseio.com";
            else if(profile.startsWith(BasicConfiguration.PRODUCTION_PROFILE.getValue()))
                return "https://lipo-passenger-production.firebaseio.com";
            else
                throw new NullPointerException("Cant get environment credentials");

        }

        @Override
        public String credentialPath(String profile) {
            String prefix = profile.startsWith(BasicConfiguration.LOCAL_PROFILE.getValue())?
                    BasicConfiguration.DEVELOP_PROFILE.getValue() : profile;
            return "firebase/passenger-"+prefix+".json";
        }
    },
    BACKOFFICE {
        @Override
        public String applicationName() { return "backoffice"; }

        @Override
        public String dataBaseUrl(String profile) {
            if(profile.startsWith(BasicConfiguration.LOCAL_PROFILE.getValue()))
                return "http://monitor.dev.backend.liporide.com/auth/validate";
            else if(profile.startsWith(BasicConfiguration.DEVELOP_PROFILE.getValue()))
                return "http://monitor.dev.backend.liporide.com/auth/validate";
            else if(profile.startsWith(BasicConfiguration.STAGE_PROFILE.getValue()))
                return "http://monitor.dev.backend.liporide.com/auth/validate";
            else if(profile.startsWith(BasicConfiguration.PRODUCTION_PROFILE.getValue()))
                return "https://monitor.prod.backend.liporide.com/auth/validate";
            else
                throw new NullPointerException("Cant get environment credentials");

        }

        @Override
        public String credentialPath(String profile) {
            return null;
        }
    };

    public abstract String applicationName();
    public abstract String dataBaseUrl(String profile);
    public abstract String credentialPath(String profile);
}
