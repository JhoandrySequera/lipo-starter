package com.liporide.parent.starter.filter.authentication.base;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class UserDtoFactory {

    private static final String USER_ID = "user_id";
    private static final String SUBJECT = "sub";
    private static final String EMAIL = "email";
    private static final String PHONE = "phone";
    private static final String AUTHORITIES = "authorities";
    private static final String INTERNAL = "Internal";

    private UserDtoFactory() {
    }

    public static LipoUserDto create(String app, Map<String, Object> claims) {
        return LipoUserDto.builder()
                .uid(claims.containsKey(USER_ID) ? claims.get(USER_ID).toString() : null)
                .subject(claims.containsKey(SUBJECT) ? claims.get(SUBJECT).toString() : null)
                .email(claims.containsKey(EMAIL) ? claims.get(EMAIL).toString() : null)
                .phone(claims.containsKey(PHONE) ? claims.get(PHONE).toString() : null)
                .project(app)
                .authorities(claims.containsKey(AUTHORITIES)
                        ? mapToGrantedAuthorities(new ArrayList<String>((Collection<String>) claims.get(AUTHORITIES)))
                        : null)
                .build();
    }

    public static LipoUserDto internalCommunication(String app) {
        return LipoUserDto.builder()
                .uid(INTERNAL)
                .subject(null)
                .email(null)
                .phone(null)
                .project(app)
                .authorities(null)
                .build();
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<String> authorities) {
        return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority))
                .collect(Collectors.toList());
    }
}
