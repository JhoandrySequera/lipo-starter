package com.liporide.parent.starter.filter.authentication.restClient;

import com.liporide.parent.starter.filter.firebase.AuthorizerConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class BackOfficeRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackOfficeRestClient.class);

    @Value("${spring.profiles.active}")
    private String profile;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    public RestOperations getRestTemplate() {
        return restTemplateBuilder.build();
    }

    public Optional<Map<String, Object>> isValid(String token) {
        try {
            ResponseEntity<String> response = getRestTemplate().postForEntity(
                    AuthorizerConfiguration.BACKOFFICE.dataBaseUrl(profile),
                    getEntityWithHeaders(token),
                    String.class);
            if (response != null && response.getStatusCode() == HttpStatus.OK) {
                return Optional.of(new HashMap());
            }
        } catch (RestClientException e) {
            LOGGER.error("Error connecting with backoffice api error -> " + e.getMessage(), e);
        }
        return Optional.empty();
    }

    public HttpEntity getEntityWithHeaders(String token){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("Accept", MediaType.ALL_VALUE);
        headers.set("Authorization", token);
        return new HttpEntity<>(headers);
    }
}
