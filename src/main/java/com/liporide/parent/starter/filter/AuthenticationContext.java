package com.liporide.parent.starter.filter;

import com.liporide.parent.starter.filter.authentication.base.LipoUserDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationContext {

    public static LipoUserDto getPrincipal() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (LipoUserDto) auth.getPrincipal();
    }
}
