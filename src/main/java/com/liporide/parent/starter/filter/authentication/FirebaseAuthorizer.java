package com.liporide.parent.starter.filter.authentication;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class FirebaseAuthorizer {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirebaseAuthorizer.class);

    public Optional<Map<String, Object>> validateToken(FirebaseAuth auth, String token) {
        try {
            FirebaseToken decodedToken = auth.verifyIdToken(token);
            return Optional.of(decodedToken.getClaims());
        } catch (FirebaseAuthException e) {
            LOGGER.error("Error validating firebase token, error -> "+ e.getMessage(), e);
        }

        return Optional.empty();
    }
}
