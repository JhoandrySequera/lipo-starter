package com.liporide.parent.starter.filter.configuration;

import com.google.api.client.util.Strings;
import com.liporide.parent.starter.filter.authentication.JWTOptions;
import com.liporide.parent.starter.filter.authentication.base.LipoUserDto;
import com.liporide.parent.starter.filter.authentication.base.UserDtoFactory;
import com.liporide.parent.starter.util.BasicConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@Service
public class WebSecurityFilter extends OncePerRequestFilter {

    private final JWTOptions jwtOptions;

    public WebSecurityFilter(@Autowired JWTOptions jwtOptions) {
        this.jwtOptions = jwtOptions;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        String authToken = request.getHeader(BasicConfiguration.HEADER_AUTH.getValue());
        String appName = request.getHeader(BasicConfiguration.HEADER_APP.getValue());

        if(!Strings.isNullOrEmpty(appName) && isAMicroserviceCommunication(appName)) {
            //Creating an user to save in context for internal microservice communication
            LipoUserDto lipoUserDto = UserDtoFactory.internalCommunication(appName);
            setSecurityContext(lipoUserDto, request);
        }else if (!Strings.isNullOrEmpty(authToken)) {
            //here do a validation depends of application name
            Optional<Map<String, Object>> claims = jwtOptions.validate(appName, authToken);

            if (claims.isPresent() && SecurityContextHolder.getContext().getAuthentication() == null) {
                LipoUserDto lipoUserDto = UserDtoFactory.create(appName, claims.get());
                setSecurityContext(lipoUserDto, request);
            }
        }
        chain.doFilter(request, response);
    }

    private boolean isAMicroserviceCommunication(String appName){
        return appName.startsWith(BasicConfiguration.HEADER_INTERNAL_API.getValue());
    }

    private void setSecurityContext(LipoUserDto lipoUserDto, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                lipoUserDto, null, lipoUserDto.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
