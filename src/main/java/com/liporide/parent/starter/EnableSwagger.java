package com.liporide.parent.starter;

import com.liporide.parent.starter.swagger.SwaggerConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ComponentScan
@Import({SwaggerConfiguration.class})
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnableSwagger {
	String name() default "";

	String description() default "";

	String version() default "";

	String basePackage() default "";

	String[] consumes() default {"application/json", "application/xml"};

	String[] produces() default {"application/json", "application/xml"};
}