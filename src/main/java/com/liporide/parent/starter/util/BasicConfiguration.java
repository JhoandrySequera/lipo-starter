package com.liporide.parent.starter.util;

public enum BasicConfiguration {

    HEADER_AUTH("Authorization"),
    HEADER_APP("AppName"),
    HEADER_INTERNAL_API("LIPO_INTERNAL_"),
    LOCAL_PROFILE("local"),
    DEVELOP_PROFILE("dev"),
    STAGE_PROFILE("stage"),
    PRODUCTION_PROFILE("prod"),
    SLACK_TOKEN("xoxb-940800860534-1447345147457-oCSEM17zhpx4gY09vUrsGj15");

    private String value;

    BasicConfiguration(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
