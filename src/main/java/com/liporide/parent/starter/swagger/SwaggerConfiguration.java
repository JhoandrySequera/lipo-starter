package com.liporide.parent.starter.swagger;

import com.google.common.base.Predicates;
import com.liporide.parent.starter.EnableSwagger;
import com.liporide.parent.starter.util.BasicConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;

@Configuration
@EnableSwagger2
@Profile("!prod")
public class SwaggerConfiguration implements ImportAware {

    public static final Contact DEFAULT_CONTACT = new Contact(
            "Lipo Backend Team", "http://www.liporide.com", "http://www.liporide.com");

    private String name = "";
    private String description = "";
    private String version = "";
    private String basePackage = "";
    private String[] consumes = {};
    private String[] produces = {};

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        Map<String, Object> annotationAttributes = importMetadata.getAnnotationAttributes(EnableSwagger.class.getName(), false);
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(annotationAttributes);
        name = attributes.getString("name");
        description = attributes.getString("description");
        version = attributes.getString("version");
        basePackage = attributes.getString("basePackage");
        consumes = attributes.getStringArray("consumes");
        produces = attributes.getStringArray("produces");
        api();
    }

    @Bean
    public Docket api() {
        Docket consumes = new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(Predicates.not(PathSelectors.regex("/actuator.*")))
                .paths(Predicates.not(PathSelectors.regex("/error.*"))).build()
                .pathMapping("")
                .globalOperationParameters(getAuthorizationHeader())
                .apiInfo(getApiInfo())
                .produces(getProduces())
                .consumes(getConsumes());
        return consumes;
    }

    public ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title(name)
                .description(description)
                .termsOfServiceUrl("http://www.apache.org/licenses/LICENSE-2.0")
                .contact(DEFAULT_CONTACT)
                .version(version)
                .build();
    }

    private Set<String> getConsumes() {
        return new HashSet<String>(Arrays.asList(consumes));
    }

    private Set<String> getProduces() {
        return new HashSet<String>(Arrays.asList(produces));
    }

    private List<Parameter> getAuthorizationHeader() {
        Parameter authToken = createParameter(BasicConfiguration.HEADER_AUTH.getValue(), "auth token", "header", true);
        Parameter appName = createParameter(BasicConfiguration.HEADER_APP.getValue(), "app name", "header", true);
        return Arrays.asList(authToken, appName);
    }

    protected Parameter createParameter(String name, String description, String parameterType, boolean required) {
        return new ParameterBuilder()
                .name(name)
                .modelRef(new ModelRef("string"))
                .required(required)
                .parameterType(parameterType)
                .description(description)
                .build();
    }
}
