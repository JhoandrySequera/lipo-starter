package com.liporide.parent.starter.slack;

public enum NotificationType {

    ERROR {
        @Override
        public String color() { return "#FF0000"; }

        @Override
        public String icon() { return " :error: "; }
    },
    WARNING {
        @Override
        public String color() { return "#FF9700"; }

        @Override
        public String icon() { return " :light: "; }
    },
    INFO {
        @Override
        public String color() { return "#00CDFF"; }

        @Override
        public String icon() { return " :info: "; }
    },
    SUCCESS {
        @Override
        public String color() { return "#00FF7C"; }

        @Override
        public String icon() { return " :check: "; }
    };

    public abstract String color();
    public abstract String icon();

}
