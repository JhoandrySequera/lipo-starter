package com.liporide.parent.starter.slack;

import com.hubspot.slack.client.SlackClient;
import com.hubspot.slack.client.SlackClientFactory;
import com.hubspot.slack.client.SlackClientRuntimeConfig;
import com.liporide.parent.starter.util.BasicConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@ComponentScan
public class SlackConfiguration {

    private SlackClientRuntimeConfig runtimeConfig;

    @PostConstruct
    public void slackInit() {
        runtimeConfig = SlackClientRuntimeConfig.builder()
                .setTokenSupplier(() -> BasicConfiguration.SLACK_TOKEN.getValue())
                .build();
    }

    @Bean(name = "slackNotificationClient")
    public SlackClient slackClient() {
        return SlackClientFactory.defaultFactory().build(runtimeConfig);
    }
}
