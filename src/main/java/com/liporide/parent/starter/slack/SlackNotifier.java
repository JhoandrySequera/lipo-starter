package com.liporide.parent.starter.slack;

import com.hubspot.slack.client.SlackClient;
import com.hubspot.slack.client.methods.params.chat.ChatPostMessageParams;
import com.hubspot.slack.client.models.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collections;

import static com.liporide.parent.starter.slack.NotificationType.*;

@Component
public class SlackNotifier {

    private static final String BOOT_NAME = "Arturito";
    private static final String BOOT_ICON = ":arturito:";

    @Autowired
    @Qualifier("slackNotificationClient")
    SlackClient slackClient;

    public void notifyError(String channel, String author, String titleMessage, String detail){
        sendBasicMessage(ERROR, channel, author, titleMessage, detail);
    }

    public void notifyWarning(String channel, String author, String titleMessage, String detail){
        sendBasicMessage(WARNING, channel, author, titleMessage, detail);
    }

    public void notifyInfo(String channel, String author, String titleMessage, String detail){
        sendBasicMessage(INFO, channel, author, titleMessage, detail);
    }

    public void notifySuccess(String channel, String author, String titleMessage, String detail){
        sendBasicMessage(SUCCESS, channel, author, titleMessage, detail);
    }

    private void sendBasicMessage(NotificationType type, String channel, String author, String titleMessage, String detail){
        Attachment attachment = Attachment.builder()
                .setAuthorName(author)
                .setColor(type.color())
                .setText(type.icon() + detail)
                .build();

        slackClient.postMessage(
                ChatPostMessageParams.builder()
                        .setUsername(BOOT_NAME)
                        .setIconEmoji(BOOT_ICON)
                        .setText(titleMessage)
                        .setChannelId(channel)
                        .setAttachments(Collections.singleton(attachment))
                        .build()
        ).join().unwrapOrElseThrow();
    }
}
